# Uop Linux Shutdown Tool #

---------

Shutdown automagically if a user of the GCE instance is not logged in.

Basically the scripts runs the following checks:

1) If nobody is logged into the system, shut the server down after 30 mins.

2) Point #1 can be negated by the user by putting in a file called uop-cancel in their homedir. This causes the server to stay up indefinitely.

3) Point #2 can be negated by the admin by putting in a file called uop-cancel in /etc/uop/. This causes the server to be shut down.

---------

**What to do with the contents of this repo.**

Copy the uop folder to /etc
copy the uop-timeout-cron to /etc/cron.d/

Then you can reap the rewards!